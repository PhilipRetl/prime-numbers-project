# Primes Numbers Project
### by: Andres vega - afelipe.vega@gmail.com
### cel : 3012017499

- Instalación => 
  - docker-compose build && docker-compose up -d
  - docker exec -it legops_back /bin/bash
  - dentro del contenedor:
    - composer install
    - npm install
    - cp .env.example .env
    - php artisan key:generate
    - php artisan migrate
    - php artisan queue:work --queue=listeners 

- .env file => 
  
   ```
      APP_NAME=Laravel
      APP_ENV=local
      APP_KEY=base64:yFa4thRLOWjuvEqIXGvBw5Y+c+9EIn1QRGSy2rThaXU=
      APP_DEBUG=true
      APP_URL=http://localhost
    
      LOG_CHANNEL=stack
      LOG_LEVEL=debug
    
      DB_CONNECTION=mysql
      DB_HOST=legops_db
      DB_DATABASE=back-db
      DB_USERNAME=philipretl
      DB_PASSWORD=holamundo
    
      BROADCAST_DRIVER=log
      CACHE_DRIVER=file
      FILESYSTEM_DRIVER=local
      QUEUE_CONNECTION=database
      SESSION_DRIVER=file
      SESSION_LIFETIME=120
    
      MEMCACHED_HOST=127.0.0.1
    
      REDIS_CLIENT=predis
      REDIS_HOST=legops-redis
      REDIS_PASSWORD=null
      REDIS_PORT=6379
    
      MAIL_MAILER=smtp
      MAIL_HOST=mailhog
      MAIL_PORT=1025
      MAIL_USERNAME=null
      MAIL_PASSWORD=null
      MAIL_ENCRYPTION=null
      MAIL_FROM_ADDRESS=null
      MAIL_FROM_NAME="${APP_NAME}"

      AWS_ACCESS_KEY_ID=
      AWS_SECRET_ACCESS_KEY=
      AWS_DEFAULT_REGION=us-east-1
      AWS_BUCKET=
      AWS_USE_PATH_STYLE_ENDPOINT=false

      PUSHER_APP_ID=
      PUSHER_APP_KEY=
      PUSHER_APP_SECRET=
      PUSHER_APP_CLUSTER=mt1

      MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
      MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

    ```
Endpoints => 
- GET: http://localhost:8084/api/v1/calculate_primes/{executions}
  - Ejemplo: http://localhost:8084/api/v1/calculate_primes/10000
  - Respuesta:
    ````javascript
       {
         "status": "processing",
         "executions": "10000",
         "uuid": "c19e13cd-78f5-4685-9a51-3fbbe1c7900c"
       }
    ````
- GET: http://localhost:8084/api/v1/primes_numbers_by_uuid/{uuid}
  - Ejemplo: http://localhost:8084/api/v1/primes_numbers_by_uuid/c19e13cd-78f5-4685-9a51-3fbbe1c7900c
  - Respuesta:
  ````javascript
    {
      "status": "success",
      "uuid": "c19e13cd-78f5-4685-9a51-3fbbe1c7900c",
      "primes_counted": 23,
      "message": "FOUND"
    }
  ````
