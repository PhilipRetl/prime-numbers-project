<?php

namespace App\Listeners;

use App\Events\CalculatePrimesEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Redis;


class CalculatePrimesListener implements ShouldQueue
{
    public $queue = 'listeners';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(CalculatePrimesEvent $event)
    {
        $this->fastPrimes($event->actual_execution, $event->executions, $event->uuid);
    }

    private function fastPrimes($actual_execution, $executions, $uuid)
    {

        $value = Redis::executeRaw(['zrangebyscore', $uuid, $actual_execution, $actual_execution, 'withscores']);

        if (empty($value)) {
            Redis::executeRaw(['zadd', $uuid, $actual_execution, $actual_execution . '_true']);
            for ($j = $actual_execution + $actual_execution; $j <= $executions; $j = $j + $actual_execution) {
                Redis::executeRaw(['zadd', $uuid, $j, $j . '_false']);
            }
        }
    }
}
