<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CalculatePrimesEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public int $executions;
    public int $actual_execution;
    public  String $uuid;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $actual_execution, int $executions, String $uuid)
    {
        $this->executions = $executions;
        $this->actual_execution = $actual_execution;
        $this->uuid = $uuid;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
