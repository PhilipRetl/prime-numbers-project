<?php

namespace App\Http\Controllers\Api;

use App\Events\CalculatePrimesEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redis;


class PrimesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($uuid)
    {
        $array_data = collect(Redis::executeRaw(['zrangebyscore', $uuid, '-inf', '+inf', 'WITHSCORES']));

        if(empty($array_data)){
            return response()->json([
                'status' => 'fail',
                'uuid' => $uuid,
                'message' => 'NOT_FOUND'
            ], 200);
        }
        $counted = $array_data->countBy(function ($prime) {
            return substr(strrchr($prime, "_"), 1);
        });


        return response()->json([
            'status' => 'success',
            'uuid' => $uuid,
            'primes_counted' => key_exists('true', $counted->toArray()) ? $counted['true'] : 0,
            'message' => 'FOUND'
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($executions)
    {
        $uuid = Str::uuid();

        dispatch(function () use ($executions, $uuid) {
            for ($actual_execution = 2; $actual_execution < $executions; $actual_execution++) {
                CalculatePrimesEvent::dispatch($actual_execution, $executions, $uuid);
            }
        })->afterResponse();

        return response()->json([
            'status' => 'processing',
            'executions' => $executions,
            'uuid' => $uuid
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
